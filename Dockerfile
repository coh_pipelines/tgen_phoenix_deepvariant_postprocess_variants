FROM google/deepvariant:0.9.0 as opt_deepvariant

RUN apt update -y && \
    apt install -y --no-install-recommends bcftools && \
    apt autoremove -y && \
    apt clean

RUN mkdir -p /usr/bin/ && echo "#!/bin/bash" > /usr/bin/module && chmod a+x /usr/bin/module

ENV PATH /opt/deepvariant/bin/:/opt/bin/:${PATH}
ENV LD_LIBRARY_PATH /opt/lib64/:/opt/lib/:${LD_LIBRARY_PATH}

